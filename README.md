# Kafka Demo Project

This is a demo project to understand Kafka producer/consumer mechanics.

## Instructions

In order to execute the project is necessary to start the kafka server using docker-compose

```
docker-compose -f kafka-server/docker-compose.yml up
```

After that, Kakfa UI will be accessible on http://127.0.0.1:3030 .

To see the producer/consumer in action is required to create the topic "first_topic"

```
docker exec -it <kafka-container-id-started> bash
kafka-topics --zookeeper 127.0.0.1:2181 --create --topic first_topic --partitions 3 --replication-factor 1
```

Then execute the producer/consumer in any order and see the magic !!!

(Do not forget of install project`s maven dependencies before execute)